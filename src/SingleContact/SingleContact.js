import React from "react"
import TableRow from "@material-ui/core/TableRow"
import TableCell from "@material-ui/core/TableCell"
import Checkbox from "@material-ui/core/Checkbox"
import { Avatar } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    cell: {
        align: 'left',
        width: '20%'
    },
    wideCell: {
        align: 'left',
        width: '60%'
    }
}))

const SingleContact = ({ handleClick, isItemSelected, contact, labelId }) => {
    const { avatar, id, first_name, last_name } = contact
    const classes = useStyles()
    return (
        <TableRow
            hover
            onClick={() => handleClick(contact)}
            role="checkbox"
            aria-checked={ isItemSelected }
            tabIndex={ -1 }
            key={ id }
            selected={ isItemSelected }
        >
            <TableCell padding="checkbox">
                <Checkbox
                    checked={ isItemSelected }
                    inputProps={ { 'aria-labelledby': labelId } }
                />
            </TableCell>
            <TableCell component="td" id={ labelId } scope="row" padding="none">
                <Avatar style={ { border: '1px solid gray' } }
                        alt={ `${ first_name }-${ last_name }` }
                        src={ avatar }/>
            </TableCell>
            <TableCell className={classes.cell}>{ first_name }</TableCell>
            <TableCell className={classes.wideCell}>{ last_name }</TableCell>
        </TableRow>
    )

}

export default SingleContact
