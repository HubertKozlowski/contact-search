import React, { useEffect, useState } from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Checkbox from '@material-ui/core/Checkbox'
import TablePagination from "@material-ui/core/TablePagination"
import SearchBar from "../SearchBar/SearchBar"
import SingleContact from "../SingleContact/SingleContact"
import { fetchContacts } from "../service"
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    root: {
        width: '700px',
        margin: '5rem auto'
    },
    paginator: {
        alignContent: 'end'
    }
}))

const ContactsList = () => {
    const classes = useStyles()

    const [defaultContacts, setDefaultContacts] = useState([])
    const [rows, setRows] = useState([])
    const [selected, setSelected] = useState([])
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(5)
    const [isInitialMount, setIsInitialMount] = useState(true)

    useEffect(() => {
        if (isInitialMount) {
            fetchContacts().then(data => {
                setDefaultContacts([...data])
                setRows([...data])
            })
            setIsInitialMount(false)
        } else {
            console.log('Selected people IDs: ', selected)
        }
    }, [selected])

    const handleClick = ({ id }) => {
        const selectedIndex = selected.indexOf(id)
        let newSelected = []
        
        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id)
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1))
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1))
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            )
        }
        setSelected(newSelected)
    }

    const handleSelectAllClick = ({ target: { checked } }) => {
        if (checked) {
            const newSelected = rows.map(n => n.id)
            setSelected(newSelected)
            return
        }
        setSelected([])
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = ({ target: { value } }) => {
        setRowsPerPage(parseInt(value, 10))
        setPage(0)
    }

    const handleInputChange = event => {
        event.persist()
        const { value } = event.target
        const filteredContacts = [...defaultContacts].filter(contact => {
            const { first_name, last_name } = contact
            return first_name.toLowerCase().includes(value.toLowerCase()) || last_name.toLowerCase().includes(value.toLowerCase())
        })
        setRows(filteredContacts)
    }

    const isSelected = id => selected.indexOf(id) !== -1

    return (
        <div className={ classes.root }>
            <Paper>
                <TableContainer>
                    <Table
                        aria-labelledby="tableTitle"
                        size='medium'
                        aria-label="enhanced table"
                    >
                        <TableHead>
                            <TableRow>
                                <TableCell padding="checkbox">
                                    <Checkbox
                                        indeterminate={ selected.length > 0 && selected < rows.length }
                                        checked={ rows.length > 0 && selected.length === rows.length }
                                        onChange={ handleSelectAllClick }
                                        inputProps={ { 'aria-label': 'select all desserts' } }
                                    />
                                </TableCell>
                                <TableCell>
                                    <h1> Contacts </h1>
                                </TableCell>
                                <TableCell/>
                                <TableCell>
                                    <SearchBar handleInputChange={ handleInputChange }/>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            { rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.id)
                                    const labelId = `enhanced-table-checkbox-${ index }`
                                    return <SingleContact
                                        key={ `contact-${ row.id }` }
                                        handleClick={ handleClick }
                                        isItemSelected={ isItemSelected }
                                        contact={ row }
                                        labelId={ labelId }/>
                                }) }
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    className={ classes.paginator }
                    component='div'
                    rowsPerPageOptions={ [5, 10, 25, 50] }
                    count={ rows.length }
                    rowsPerPage={ rowsPerPage }
                    page={ page }
                    onChangePage={ handleChangePage }
                    onChangeRowsPerPage={ handleChangeRowsPerPage }
                />
            </Paper>
        </div>
    )
}
export default ContactsList
