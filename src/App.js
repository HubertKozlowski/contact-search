import React from "react"
import ContactsList from "./ContactsList/ContactsList"
import './App.css'

const App = () => {
    return <ContactsList />
}

export default App
