import axios from "axios"

export const fetchContacts = () => {
    const URL = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json'
    return axios
        .get(URL )
        .then(res => res.data)
        .catch(err => console.log("Error: ", err));
}
