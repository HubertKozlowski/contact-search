# Contact Search

## How to run

Copy and connect the repository locally so that you can push updates you make and pull changes others make. Enter git clone and the repository URL on the command line:

#### `git clone https://gitlab.com/HubertKozlowski/contact-search.git`

In the project directory, you can run:

#### `npm install`

and 

#### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
